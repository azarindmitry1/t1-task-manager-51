package ru.t1.azarin.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;
import ru.t1.azarin.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskListRequest(@Nullable final String token) {
        super(token);
    }

}
